﻿using System;
using NUnit.Framework;
using OnlyOne;

namespace OnlyOneTests
{
    [TestFixture]
    public class KataTests
    {
        private bool[] _flags;
        
        [Test]
        public void OnlyOneTest_one_true_should_response_true()
        {
            GivenFlags(true);
            ShouldResponse(true);
        }
        
        [Test]
        public void OnlyOneTest_zero_true_should_response_false()
        {
            GivenFlags(false);
            ShouldResponse(false);
        }
        
        [Test]
        public void OnlyOneTest_null_should_response_false()
        {
            GivenFlags(null);
            ShouldResponse(false);
        }
        
        [Test]
        public void OnlyOneTest_two_true_should_response_false()
        {
            GivenFlags(true, true);
            ShouldResponse(false);
        }

        private void GivenFlags(params bool[] flags)
        {
            _flags = flags;
        }

        private void ShouldResponse(bool expected)
        {
            Assert.AreEqual(expected, Kata.OnlyOne(_flags));
        }
    }
}