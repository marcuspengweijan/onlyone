﻿using System.Linq;

namespace OnlyOne
{
    public class Kata
    {
        public static bool OnlyOne(params bool[] flags)
        {
            return flags != null && flags.Count(x => x) == 1;
        }
    }
}